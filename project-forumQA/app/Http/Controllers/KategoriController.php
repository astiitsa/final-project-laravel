<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KategoriController extends Controller
{
    public function home(){
        return view('welcome');
    }

    public function create()
    {
        return view('kategori.tambah');
    }

    public function store (Request $request)
    {
        $request->validate([
            'nama_kategori' => 'required',
        ]);
        DB::table('kategori')->insert([
            "nama_kategori" => $request["nama_kategori"]
        ]);
        return redirect('/kategori');
    }

    public function index()
    {
    $kategori = DB::table('kategori')->get();
    return view('kategori.tampil',['kategori' => $kategori]);
    }

    public function show($id)
    {
    $kategori = DB::table('kategori')->find($id);
    return view('kategori.detail',['kategori' => $kategori]);
    }

    public function edit($id)
        {
        $kategori = DB::table('kategori')->find($id);
        return view('kategori.edit',['kategori' => $kategori]);
        }

    public function update($id,Request $request)
        {
            $request->validate([
                'nama_kategori' => 'required',
            ]);
        
            DB::table('kategori')
                ->where('id', $id)
                ->update(
                    [
                        'nama_kategori' => $request['nama_kategori']
                    ]
                    );
            return redirect('/kategori');
        }

    public function destroy($id)
        {
            DB::table('kategori')->where('id','=', $id)->delete();
            return redirect('/kategori');
        }

}
