<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;
use App\Models\Pertanyaan;
use File;
use Illuminate\Support\Facades\Auth;
use Cron\MinutesField;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pertanyaan = Pertanyaan::all();
        return view('pertanyaan.tampil', compact('pertanyaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = kategori::all();
        return view('pertanyaan.tambah',['kategori' => $kategori]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'gambar' => 'required| mimes:jpg,jpeg,png', 
            'kategori_id' => 'required',
        ]);

        $id = Auth::id();

        $imageName = time().'.'.$request->image->extension();  
        $request->gambar->move(public_path('images'), $imageName);

        Pertanyaan::create([
            'judul' => $request['judul'],
            'content' => $request['content'],
            'gambar' => $imageName,
            'kategori_id' => $request['kategori_id'],
            'user_id' => $request['user_id']
        ]);
        
        return redirect('/Pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        return view('pertanyaan.detail', ['pertanyaan' => $pertanyaan]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pertanyaan= Pertanyaan::find($id);
        $kategori = Kategori::all();

        return view('pertanyaan.id',['pertanyaan' => $pertanyaan, 'kategori' => $kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'gambar' => 'required| mimes:jpg,jpeg,png', 
            'kategori_id' => 'required',
        ]);

        $pertanyaan = Pertanyaan::find($id);

        if($request->has('gambar')) {
            $path = "images/";
            File::delete($path . $pertanyaan->gambar);
            

            $namaGambar = time().'.'.$request->gambar->extension();  
   
            $request->gambar->move(public_path('images'), $namaGambar);  

            $pertanyaan->gambar = $namaGambar;

            $pertanyaan->save();
        }

        $pertanyaan->judul = $request->judul;
        $pertanyaan->content = $request->content;
        $pertanyaan->kategori_id = $request->kategori_id;

        $pertanyaan->save();
        return redirect('/pertanyaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $path = "images/";
        File::delete($path . $pertanyaan->gambar);

        $pertanyaan->delete();

        return redirect('/pertanyaan');
    }
}
