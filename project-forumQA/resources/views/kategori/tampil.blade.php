@extends('layouts.master')
@section('title')
    Kategori
@endsection
@section('content')
<a href="/kategori/create" class="btn btn-primary btn-sm my-2">Tambah</a>

        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Kategori</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
                @forelse ($kategori as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama_kategori}}</td>
                        <td>   
                        <form action="/kategori/{{$value->id}}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="/kategori/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/kategori/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                            </form>
                        </td>     
                    </tr>
                @empty
                    <tr colspan="5">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection