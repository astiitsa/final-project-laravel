@extends('layouts.master')
@section('title')
    Detail Pertanyaan
@endsection
@section('content')
<img src="{{asset('image/'.$pertanyaan->gambar)}}" alt="" width="100%" height="300px">
<h3 class="text-primary">{{$pertanyaan->judul}}</h3>
<p>{{$pertanyaan->content}}</p>
@endsection