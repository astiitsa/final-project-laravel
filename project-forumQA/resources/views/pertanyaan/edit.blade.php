@extends('layouts.master')
@section('title')
    Edit Pertanyaan
@endsection
@section('content')
<form action="/pertanyaan" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Judul</label>
        <input type="text" class="form-control" name="judul" value="{{$pertanyaan->judul}}" placeholder="Masukkan Judul Pertanyaan">
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Content</label>
        <textarea name="content" class="form-control" cols="30" rows="20">{{$pertanyaan->content}}</textarea>
        @error('content')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Gambar</label>
        <input type="file" class="form-control" name="gambar">
        @error('gambar')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Kategori</label>
        <select name="kategori_id" class="form-control" id="">
            <option value="">--Pilih Kategori-</option>
            @forelse ($kategori as $item)
            @if ($item->id == $pertanyaan->kategori_id)
            <option value="{{$item->id}}" selected>{{$item->nama_kategori}}</option>
            @else
            <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
            @endif
                
            @empty
            <option value="">Tidak ada data di kategori</option>
            @endforelse
        </select>
        @error('kategori_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection