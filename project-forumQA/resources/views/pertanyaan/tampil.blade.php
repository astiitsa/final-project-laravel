@extends('layouts.master')
@section('title')
    Pertanyaan
@endsection
@section('content')
<a href="/pertanyaan/create" class="btn btn-primary btn-sm my-2">Tambah</a>

<div class="row">
    @forelse($pertanyaan as $item)
    <div class="col-4">
    <div class="card">
        <img src="{{asset('images/'.$item->gambar)}}" class="card-img-top" style="height: 300px" alt="...">
        <div class="card-body">
          <h3 class="card-title">{{$item->judul}}</h>
          <p class="card-text">{{str::limit($item->content,50)}}</p>
          <a href="/pertanyaan/{{$item->id}}" class="btn btn-info btn-sm btn-block">Detail</a>
          <div class="row mt-3">
            <div class="col"></div>
            <a href="/pertanyaan/{{$item->id}}" class="btn btn-warning btn-sm btn-block">Edit</a>
          </div>
          <div class="col">
            <form action="/pertanyaan/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                <input type="submit" value="Delete" class="btn btn-danger btn-sm btn-block">
          </div>
        </div>
    </div>
</div>
    @empty
    <h3>Tidak ada pertanyaan</h3>
    @endforelse
</div>
@endsection