<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\PertanyaanController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [KategoriController::class, 'home']);
//CRUD
//Create Data
Route::get('/kategori/create',[KategoriController::class,'create']);
Route::post('/kategori',[KategoriController::class,'store']);

//Read Data
Route::get('/kategori',[KategoriController::class,'index']);
Route::get('/kategori/{id}',[KategoriController::class,'show']);

//Update Data
Route::get('/kategori/{id}/edit',[KategoriController::class,'edit']);
Route::put('/kategori/{id}',[KategoriController::class,'update']);

//Delete Data
Route::delete('/kategori/{id}',[KategoriController::class,'destroy']);

Route::resource('pertanyaan', PertanyaanController::class);

Auth::routes();

