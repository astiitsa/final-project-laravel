<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
    use HasFactory;
    protected $table = "profile";
    protected $fillable =['biodata','umur','alamat','user_id'];

    public function users() {
        return $this->belongsTo('App\User', 'users_id');
    }
}