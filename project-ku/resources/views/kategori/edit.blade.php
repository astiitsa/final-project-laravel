@extends('layouts.master')
@section('title')
    Halaman Edit Kategori
@endsection
@section('content')
    <form action="/kategori/{{$kategori->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama_kategori">Nama Kategori</label>
            <input type="text" class="form-control" name="nama_kategori" value="{{$kategori->nama_kategori}}" placeholder="Masukkan Nama Kategori">
            @error('nama_kategori')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection