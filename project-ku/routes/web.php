<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [kategoriController::class, 'home']);

//CRUD
//Create Data
Route::get('/kategori/create',[kategoriController::class,'create']);
Route::post('/kategori',[kategoriController::class,'store']);

//Read Data
Route::get('/kategori',[kategoriController::class,'index']);
Route::get('/kategori/{id}',[kategoriController::class,'show']);

//Update Data
Route::get('/kategori/{id}/edit',[kategoriController::class,'edit']);
Route::put('/kategori/{id}',[kategoriController::class,'update']);

//Delete Data
Route::delete('/kategori/{id}',[kategoriController::class,'destroy']);